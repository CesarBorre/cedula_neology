package c.neo.cedula2.fragments;

import java.util.Date;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.neo.cedula2.R;
import c.neo.cedula2.utils.Cedula;
import c.neo.cedula2.utils.DateUtil;
import c.neo.cedula2.utils.ImageUtil;

public class FrenteFragment extends Fragment {
	TextView tvUI;
	Cedula cedula;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		cedula = (Cedula) getArguments().get("cedula");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_frente, container, false);

		setUIDatosFrente(cedula, rootView);

		return rootView;
	}

	@SuppressLint("NewApi")
	public void setUIDatosFrente(Cedula cedula, View rootView) {
		ViewHolder viewHolder = new ViewHolder();

		viewHolder.contenedor = (LinearLayout) rootView.findViewById(R.id.contenedorPrincipal);
		viewHolder.foto = (ImageView) rootView.findViewById(R.id.foto_img);
		ImageUtil.setImg(cedula.getStrURLfoto(), viewHolder.foto);
		viewHolder.cedulaLbl = (TextView) rootView.findViewById(R.id.lblCedulaID);
		viewHolder.cedulaTxt = (TextView) rootView.findViewById(R.id.txtCedulaID);
		viewHolder.cedulaTxt.setText(String.valueOf(cedula.getIntNoCedula()));
		viewHolder.nombreLbl = (TextView) rootView.findViewById(R.id.label_nombre);
		viewHolder.nombreTxt = (TextView) rootView.findViewById(R.id.nombre);
		viewHolder.nombreTxt.setText(cedula.getStrNombre());
		viewHolder.apellidosLbl = (TextView) rootView.findViewById(R.id.label_apellidos);
		viewHolder.apellidosTxt = (TextView) rootView.findViewById(R.id.apellidos);
		viewHolder.apellidosTxt.setText(cedula.getStrApellidos());
		viewHolder.profesionLbl = (TextView) rootView.findViewById(R.id.label_profesion);
		viewHolder.profesionTxt = (TextView) rootView.findViewById(R.id.profesion);
		viewHolder.profesionTxt.setText(cedula.getStrProfesion());
		viewHolder.estadoCivilLbl = (TextView) rootView.findViewById(R.id.label_estado_civil);
		viewHolder.estadoCivilTxt = (TextView) rootView.findViewById(R.id.estado_civil);
		viewHolder.estadoCivilTxt.setText(cedula.getStrEstadoCivil());
		viewHolder.fechaNacLbl = (TextView) rootView.findViewById(R.id.label_fecha_nacimiento);
		viewHolder.fechaNacTxt = (TextView) rootView.findViewById(R.id.fecha_nacimiento);
		viewHolder.fechaNacTxt.setText(DateUtil.dateToString(new Date(cedula.getDateFechaNacimiento()), "dd-MM-yyyy"));
		viewHolder.lugarNacLbl = (TextView) rootView.findViewById(R.id.label_lugar_nacimiento);
		viewHolder.lugarNacTxt = (TextView) rootView.findViewById(R.id.lugar_nacimiento);
		viewHolder.lugarNacTxt.setText(cedula.getStrLugarNacimiento());
		viewHolder.lblMatricula = (TextView) rootView.findViewById(R.id.lblMatricula);
		viewHolder.lblMatricula.setText(cedula.getStrMatricula());
		viewHolder.fotoMini = (ImageView) rootView.findViewById(R.id.imgFantasmaID);
		

		rootView.setTag(viewHolder);

		viewHolder.fotoMini.setAlpha(85);

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 5;
		Bitmap preview_bitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.test_1, options);

		viewHolder.contenedor.setBackground(new BitmapDrawable(preview_bitmap));
	}

	static class ViewHolder {
		LinearLayout contenedor;
		ImageView fotoMini;
		TextView cedulaLbl;
		TextView cedulaTxt;
		TextView nombreLbl;
		TextView nombreTxt;
		TextView apellidosLbl;
		TextView apellidosTxt;
		TextView profesionLbl;
		TextView profesionTxt;
		TextView estadoCivilLbl;
		TextView estadoCivilTxt;
		TextView fechaNacLbl;
		TextView fechaNacTxt;
		TextView lugarNacLbl;
		TextView lugarNacTxt;
		TextView domicilioTxt;
		TextView domicilioLbl;
		TextView lblMatricula;
		ImageView foto;
	}
}
