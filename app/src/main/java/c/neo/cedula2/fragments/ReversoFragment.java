package c.neo.cedula2.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import c.neo.cedula2.R;
import c.neo.cedula2.utils.Cedula;
import c.neo.cedula2.utils.ImageUtil;

public class ReversoFragment extends Fragment {

	Cedula cedula;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		cedula = (Cedula) getArguments().get("cedula");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_reverso, container,
				false);

		setUIDatosReverso(cedula, rootView);

		return rootView;
	}

	public void setUIDatosReverso(Cedula cedula, View rootView) {

		ViewHolder viewHolder = new ViewHolder();

		viewHolder.logoNeoImg = (ImageView) rootView
				.findViewById(R.id.logoNeoID);
		viewHolder.huellaImg = (ImageView) rootView
				.findViewById(R.id.huellaImgID);
		ImageUtil.setImg(cedula.getStrURLhuella(), viewHolder.huellaImg);
		viewHolder.firmaPersonaImg = (ImageView) rootView
				.findViewById(R.id.firmaID);
		ImageUtil.setImg(cedula.getStrURLfirma(), viewHolder.firmaPersonaImg);
		viewHolder.logoSmartracImg = (ImageView) rootView
				.findViewById(R.id.logoSmartracID);
		viewHolder.firmaGabineteImg = (ImageView) rootView
				.findViewById(R.id.firmaJefeGabinete);
		viewHolder.firmaDepImg = (ImageView) rootView
				.findViewById(R.id.firmaDirDept);
		viewHolder.cedulaLbl = (TextView) rootView.findViewById(R.id.lblCedula);
		viewHolder.cedulaTxt = (TextView) rootView.findViewById(R.id.txtCedula);
		viewHolder.cedulaTxt.setText(String.valueOf(cedula.getIntNoCedula()));
		viewHolder.serieLbl = (TextView) rootView
				.findViewById(R.id.label_serie);
		viewHolder.serieTxt = (TextView) rootView.findViewById(R.id.serieFragment);
		viewHolder.serieTxt.setText(String.valueOf(cedula.getIntSerie()));
		viewHolder.seccionLbl = (TextView) rootView
				.findViewById(R.id.label_seccion);
		viewHolder.seccionTxt = (TextView) rootView.findViewById(R.id.seccion);
		viewHolder.seccionTxt.setText(String.valueOf(cedula.getIntSeccion()));
		viewHolder.lugarEmisionLbl = (TextView) rootView
				.findViewById(R.id.lugarEmisionLbl);
		viewHolder.lugarEmisionTxt = (TextView) rootView
				.findViewById(R.id.lugarEmisionTxt);
		viewHolder.lugarEmisionTxt.setText(cedula.getStrNombreEmision());
		viewHolder.jefeGabineteLbl = (TextView) rootView
				.findViewById(R.id.jefeGabineteLbl);
		viewHolder.dirDeptLbl = (TextView) rootView
				.findViewById(R.id.dirDepLbl);
		viewHolder.matriculaLbl = (TextView) rootView
				.findViewById(R.id.matriculaID);
		viewHolder.matriculaLbl.setText(cedula.getStrMatricula());
		viewHolder.ocr1 = (TextView) rootView.findViewById(R.id.ocr1ID);
		viewHolder.ocr1.setText(cedula.getStrOCR1());
		viewHolder.ocr2 = (TextView) rootView.findViewById(R.id.ocr2ID);
		viewHolder.ocr2.setText(cedula.getStrOCR2());
		viewHolder.ocr3 = (TextView) rootView.findViewById(R.id.ocr3ID);
		viewHolder.ocr3.setText(cedula.getStrOCR3());

		Typeface typeFace = Typeface.createFromAsset(getActivity()
				.getApplicationContext().getAssets(), "fonts/OCRAEXT.TTF");
		viewHolder.ocr1.setTypeface(typeFace);
		viewHolder.ocr2.setTypeface(typeFace);
		viewHolder.ocr3.setTypeface(typeFace);

		rootView.setTag(viewHolder);
	}

	static class ViewHolder {
		ImageView logoNeoImg;
		ImageView huellaImg;
		ImageView firmaPersonaImg;
		ImageView logoSmartracImg;
		ImageView firmaGabineteImg;
		ImageView firmaDepImg;
		TextView cedulaLbl;
		TextView cedulaTxt;
		TextView serieLbl;
		TextView serieTxt;
		TextView seccionLbl;
		TextView seccionTxt;
		TextView lugarEmisionLbl;
		TextView lugarEmisionTxt;
		TextView jefeGabineteLbl;
		TextView dirDeptLbl;
		TextView matriculaLbl;
		TextView ocr1;
		TextView ocr2;
		TextView ocr3;
	}
}
