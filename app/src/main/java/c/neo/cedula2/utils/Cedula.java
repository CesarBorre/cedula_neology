package c.neo.cedula2.utils;

import java.io.Serializable;

public class Cedula implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6349678615970739738L;
	long intNoCedula;
	String strMatricula;
	String dateFechaEmision;
	String dateFechaExpiracion;
	int intSerie;
	int intSeccion;
	int intLugarEmision;
	String strClaveEmision;
	String strNombreEmision;
	String strNombre;
	String strApellidos;
	String strProfesion;
	String strDomicilio;
	String strEstadoCivil;
	long dateFechaNacimiento;
	String strLugarNacimiento;
	String strOCR1;
	String strOCR2;
	String strOCR3;
	byte[] strURLfoto;
	byte[] strURLfirma;
	byte[] strURLhuella;

	public Cedula(
			long intNoCedula, 
			String strMatricula,
			String dateFechaEmision, 
			String dateFechaExpiracion, 
			int intSerie,
			int intSeccion, 
			int intLugarEmision, 
			String strClaveEmision,
			String strNombreEmision, 
			String strNombre, 
			String strApellidos,
			String strProfesion, 
			String strDomicilio, 
			String strEstadoCivil,
			long dateFechaNacimiento, 
			String strLugarNacimiento,
			String strOCR1, 
			String strOCR2, 
			String strOCR3, 
			byte[] strURLfoto,
			byte[] strURLfirma, 
			byte[] strURLhuella) {

		this.intNoCedula = intNoCedula;
		this.strMatricula = strMatricula;
		this.dateFechaEmision = dateFechaEmision;
		this.dateFechaExpiracion = dateFechaExpiracion;
		this.intSerie = intSerie;
		this.intSeccion = intSeccion;
		this.intLugarEmision = intLugarEmision;
		this.strClaveEmision = strClaveEmision;
		this.strNombreEmision = strNombreEmision;
		this.strNombre = strNombre;
		this.strApellidos = strApellidos;
		this.strProfesion = strProfesion;
		this.strDomicilio = strDomicilio;
		this.strEstadoCivil = strEstadoCivil;
		this.dateFechaNacimiento = dateFechaNacimiento;
		this.strLugarNacimiento = strLugarNacimiento;
		this.strOCR1 = strOCR1;
		this.strOCR2 = strOCR2;
		this.strOCR3 = strOCR3;
		this.strURLfoto = strURLfoto;
		this.strURLfirma = strURLfirma;
		this.strURLhuella = strURLhuella;
	}

	public long getIntNoCedula() {
		return intNoCedula;
	}

	public void setIntNoCedula(long intNoCedula) {
		this.intNoCedula = intNoCedula;
	}

	public String getStrMatricula() {
		return strMatricula;
	}

	public void setStrMatricula(String strMatricula) {
		this.strMatricula = strMatricula;
	}

	public String getDateFechaEmision() {
		return dateFechaEmision;
	}

	public void setDateFechaEmision(String dateFechaEmision) {
		this.dateFechaEmision = dateFechaEmision;
	}

	public String getDateFechaExpiracion() {
		return dateFechaExpiracion;
	}

	public void setDateFechaExpiracion(String dateFechaExpiracion) {
		this.dateFechaExpiracion = dateFechaExpiracion;
	}

	public int getIntSerie() {
		return intSerie;
	}

	public void setIntSerie(int intSerie) {
		this.intSerie = intSerie;
	}

	public int getIntSeccion() {
		return intSeccion;
	}

	public void setIntSeccion(int intSeccion) {
		this.intSeccion = intSeccion;
	}

	public int getIntLugarEmision() {
		return intLugarEmision;
	}

	public void setIntLugarEmision(int intLugarEmision) {
		this.intLugarEmision = intLugarEmision;
	}

	public String getStrClaveEmision() {
		return strClaveEmision;
	}

	public void setStrClaveEmision(String strClaveEmision) {
		this.strClaveEmision = strClaveEmision;
	}

	public String getStrNombreEmision() {
		return strNombreEmision;
	}

	public void setStrNombreEmision(String strNombreEmision) {
		this.strNombreEmision = strNombreEmision;
	}

	public String getStrNombre() {
		return strNombre;
	}

	public void setStrNombre(String strNombre) {
		this.strNombre = strNombre;
	}

	public String getStrApellidos() {
		return strApellidos;
	}

	public void setStrApellidos(String strApellidos) {
		this.strApellidos = strApellidos;
	}

	public String getStrProfesion() {
		return strProfesion;
	}

	public void setStrProfesion(String strProfesion) {
		this.strProfesion = strProfesion;
	}

	public String getStrDomicilio() {
		return strDomicilio;
	}

	public void setStrDomicilio(String strDomicilio) {
		this.strDomicilio = strDomicilio;
	}

	public String getStrEstadoCivil() {
		return strEstadoCivil;
	}

	public void setStrEstadoCivil(String strEstadoCivil) {
		this.strEstadoCivil = strEstadoCivil;
	}

	public long getDateFechaNacimiento() {
		return dateFechaNacimiento;
	}

	public void setDateFechaNacimiento(long dateFechaNacimiento) {
		this.dateFechaNacimiento = dateFechaNacimiento;
	}

	public String getStrLugarNacimiento() {
		return strLugarNacimiento;
	}

	public void setStrLugarNacimiento(String strLugarNacimiento) {
		this.strLugarNacimiento = strLugarNacimiento;
	}

	public String getStrOCR1() {
		return strOCR1;
	}

	public void setStrOCR1(String strOCR1) {
		this.strOCR1 = strOCR1;
	}

	public String getStrOCR2() {
		return strOCR2;
	}

	public void setStrOCR2(String strOCR2) {
		this.strOCR2 = strOCR2;
	}

	public String getStrOCR3() {
		return strOCR3;
	}

	public void setStrOCR3(String strOCR3) {
		this.strOCR3 = strOCR3;
	}

	public byte[] getStrURLfoto() {
		return strURLfoto;
	}

	public void setStrURLfoto(byte[] strURLfoto) {
		this.strURLfoto = strURLfoto;
	}

	public byte[] getStrURLfirma() {
		return strURLfirma;
	}

	public void setStrURLfirma(byte[] strURLfirma) {
		this.strURLfirma = strURLfirma;
	}

	public byte[] getStrURLhuella() {
		return strURLhuella;
	}

	public void setStrURLhuella(byte[] strURLhuella) {
		this.strURLhuella = strURLhuella;
	}
}
