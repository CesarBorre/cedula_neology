package c.neo.cedula2.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.ImageView;

/**
 * 
 * @author Cesar Segura
 *
 */

public class ImageUtil {

	public static void setImg(byte[] b, ImageView imageView) {
		byte[] decodedString = Base64.decode(b, Base64.DEFAULT);
		Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0,
				decodedString.length);
		imageView.setImageBitmap(bitmap);
	}
}
