package c.neo.cedula2;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.content.res.Resources;
import android.graphics.Color;
import android.nfc.NfcAdapter;
import android.nfc.tech.NfcF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import c.neo.cedula2.lecturanfc.LecturaTag;
import c.neo.cedula2.utils.Cedula;
import c.neo.cedula2.utils.CheckInternetConnection;
import c.neo.cedula2.utils.SnackBar;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private static final String URL = "http://mobile.neology-demos.com:8080/api/cedulaNeo?intNoCedula=";

    MainActivity mActivity;
    NfcAdapter nfcAdapter = null;
    PendingIntent pendingIntent = null;
    IntentFilter[] filters = null;
    String[][] techList = null;
    protected String[] datosTag;

    TextView bienvenidaTxt;
    private TextView tvUI;
    private ImageView ivUI;
    private RelativeLayout rlUI;
    LinearLayout datosNFC, vencimientoLinearLayOut;
    MenuItem search_itemUI;
    private Menu menu_mainUI;

    private String no_cedula = "";
    private boolean datosValidos = false;

    Intent lecturaIntent;
    private Toolbar mToolbar;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolBar();
        nfcController();
        enableProgressDIalog();
    }

    private void setToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("CÉDULA NEOLOGY");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_launcher);
        setSupportActionBar(mToolbar);
    }

    private void nfcController() {
        // Obtenemos el control sobre el lector de NFC
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        // Si no se encuentra el lector de NFC se cierra aplicacion
        if (nfcAdapter == null) {
            Toast.makeText(this, getString(R.string.nfc_warning),
                    Toast.LENGTH_LONG).show();
            finish();
            return;

        } else if (nfcAdapter.isEnabled()) {
            // Creamos un Intent para manejar los datos leidos
            pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                    getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

            // Creamos un filtro de Intent relacionado con descubrir un mensaje NDEF
            IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
            IntentFilter discovery = new IntentFilter(
                    NfcAdapter.ACTION_TAG_DISCOVERED);

            // Configuramos el filtro para que acepte de cualquier tipo de NDEF
            try {
                ndef.addDataType("*/*");
            } catch (MalformedMimeTypeException e) {
                throw new RuntimeException("fail", e);
            }
            filters = new IntentFilter[]{ndef, discovery};

            // Configuramos para que lea de cualquier clase de tag NFC
            techList = new String[][]{new String[]{NfcF.class.getName()}};

            Intent lecturaIntent = getIntent();
            Log.d(TAG, "INTENT " + lecturaIntent.toString());
            Log.d(TAG, "PENDING INTENT " + pendingIntent.toString());
            Parcelable[] rawMsgs = lecturaIntent
                    .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            Log.d(TAG, "RAW " + rawMsgs);

            if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(lecturaIntent.getAction())) {
                lecturaNFC(lecturaIntent);
            }
        } else {
            SnackBar.showSnackBar(getResources().getString(R.string.no_nfc), this, 0, R.id.mainID);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.data, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Log.d(TAG, "Click en Buscar");
                if (datosValidos) {
                    Log.d(TAG, "Consultar informacion");
                    if (CheckInternetConnection.isConnectedToInternet(getApplicationContext())) {
                        callWSCedula(no_cedula);
                    } else {
                        SnackBar.showSnackBar(getResources().getString(R.string.no_wifi), this, 1, R.id.mainID);
                    }
                } else {
                    SnackBar.showSnackBar("DEBE LEER UNA TARJETA NEOLOGY PRIMERO", this, 2, R.id.mainID);
                }
                break;

            default:
                break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart()");
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.d(TAG, "onResume()");
//		nfcAdapter.enableForegroundDispatch(this, pendingIntent, filters,
//				techList);
//		lecturaNFC(lecturaIntent);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG, "onPause()");
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Log.d(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "onNewIntent()");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        lecturaNFC(intent);
    }

    public void lecturaNFC(Intent intent) {
        try {

            LecturaTag lecturaObj = new LecturaTag(intent);
            datosTag = lecturaObj.lectura();

            tvUI = (TextView) findViewById(R.id.bienvenida_msg);
            tvUI.setVisibility(View.GONE);

            // rlUI = (RelativeLayout)findViewById(R.id.estado_contenedor);
            // rlUI.setVisibility(View.VISIBLE);
            //
            // rlUI = (RelativeLayout)findViewById(R.id.datos_contenedor);

            vencimientoLinearLayOut = (LinearLayout) findViewById(R.id.vencimientoLayOutID);
            vencimientoLinearLayOut.setVisibility(View.VISIBLE);

            datosNFC = (LinearLayout) findViewById(R.id.datosNFClLayOutID);

            if (datosTag != null) {

                Log.d(TAG, "# Datos leidos: " + datosTag.length);
                int i = 0;
                while (i < datosTag.length) {
                    Log.d(TAG, "Dato[" + i + "]: " + datosTag[i]);
                    i++;
                }

                tvUI = (TextView) findViewById(R.id.estado);
                ivUI = (ImageView) findViewById(R.id.estado_img);

                try {
                    Calendar c = Calendar.getInstance();

                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "DD-MM-yyyy", Locale.getDefault());
                    SimpleDateFormat outFormat = new SimpleDateFormat(
                            "DD/MM/yyyy", Locale.getDefault());
                    Date date_vencimiento = dateFormat.parse(datosTag[5]);

                    Date hoy = c.getTime();
                    datosTag[4] = datosTag[4].replace("-", "/");
                    datosTag[5] = datosTag[5].replace("-", "/");

                    if (hoy.before(date_vencimiento)) {
                        Log.d(TAG,
                                "Permiso OK: "
                                        + outFormat.format(date_vencimiento));
                        tvUI.setText(R.string.status_ok);
                        ivUI.setImageResource(R.drawable.ok);
                    } else {
                        Log.d(TAG,
                                "Permiso Vencido: "
                                        + outFormat.format(date_vencimiento));
                        tvUI.setText(R.string.status_expired);
                        ivUI.setImageResource(R.drawable.vencido);
                    }

                } catch (Exception e) {
                    Log.e(TAG,
                            "Exception::lecturaNFC:Bloque fecha->"
                                    + e.getMessage());
                }

                no_cedula = datosTag[1];
                tvUI = (TextView) findViewById(R.id.cedula);
                tvUI.setText(no_cedula);

                tvUI = (TextView) findViewById(R.id.matricula);
                tvUI.setText(datosTag[0]);

                tvUI = (TextView) findViewById(R.id.serie);
                tvUI.setText(datosTag[2]);
                tvUI = (TextView) findViewById(R.id.seccion);
                tvUI.setText(datosTag[3]);
                tvUI = (TextView) findViewById(R.id.fecha_emision);
                tvUI.setText(datosTag[4]);
                tvUI = (TextView) findViewById(R.id.fecha_vencimiento);
                tvUI.setText(datosTag[5]);

                int indice_depto = Integer.parseInt(datosTag[6]);

                Resources res = getResources();
                String[] departamentos = null;
                departamentos = res.getStringArray(R.array.departamentos);

                tvUI = (TextView) findViewById(R.id.lugar_emision);
                tvUI.setText(departamentos[indice_depto]);

                // int id_img_depto =
                // getResources().getIdentifier("depto_"+datosTag[6],
                // "drawable", getPackageName());
                int id_img_depto = getResources().getIdentifier("ic_launcher",
                        "drawable", getPackageName());

                ivUI = (ImageView) findViewById(R.id.img_depto);
                ivUI.setImageResource(id_img_depto);

                datosValidos = true;
                datosNFC.setVisibility(View.VISIBLE);

            } else {

                Log.i(TAG, "Lectura de datos no validos");

                ivUI = (ImageView) findViewById(R.id.estado_img);
                ivUI.setImageResource(R.drawable.error);

                tvUI = (TextView) findViewById(R.id.estado);
                tvUI.setText(R.string.status_invalid);

                datosValidos = false;
                rlUI.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e(TAG, "Excepion::lecturaNFC->" + e.getMessage());
            e.printStackTrace();
        }

    }

    private void enableProgressDIalog() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.wait_label));
        dialog.setTitle(getString(R.string.gettingdata_label));
        dialog.setCanceledOnTouchOutside(false);
    }

    private void callWSCedula(String intNoCedula) {
        dialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, URL + intNoCedula, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        new parseJson().execute(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                Toast.makeText(getApplicationContext(), "Error en la consulta, intentar de nuevo", Toast.LENGTH_SHORT).show();
            }
        });
        VolleyApp.getmInstance().addToRequestQueue(jsonObjectRequest);
    }

    private class parseJson extends AsyncTask<JSONObject, Void, Cedula> {
        Cedula cedula = null;

        @Override
        protected Cedula doInBackground(JSONObject... params) {
            try {
                if (params[0].getInt("code") == 200) {
                    JSONObject cedulaObject = (JSONObject) params[0].get("cedulasNeology");
                    JSONObject estadosObject = cedulaObject.getJSONObject("estadosMexico");
                    JSONObject personasObject = cedulaObject.getJSONObject("personas");

                    cedula = new Cedula(
                            cedulaObject.getLong("intNoCedula"),
                            cedulaObject.getString("strMatricula"),
                            cedulaObject.getString("dateFechaEmision"),
                            cedulaObject.getString("dateFechaExpiracion"),
                            cedulaObject.getInt("intSerie"),
                            cedulaObject.getInt("intSeccion"),
                            estadosObject.getInt("intID"),
                            estadosObject.getString("strClave"),
                            estadosObject.getString("strNombre"),
                            personasObject.getString("strNombre"),
                            personasObject.getString("strApellidos"),
                            personasObject.getString("strProfesion"),
                            personasObject.getString("strDomicilio"),
                            personasObject.getString("strEstadoCivil"),
                            personasObject.getLong("dateFechaNacimiento"),
                            personasObject.getString("strLugarNacimiento"),
                            personasObject.getString("strOCR1"),
                            personasObject.getString("strOCR2"),
                            personasObject.getString("strOCR3"),
                            personasObject.getString("strURLfoto").getBytes(),
                            personasObject.getString("strURLfirma").getBytes(),
                            personasObject.getString("strURLhuella").getBytes());

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return cedula;
        }

        @Override
        protected void onPostExecute(Cedula result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (result != null) {
                Intent intent = new Intent(MainActivity.this,
                        DataActivity.class);
                intent.putExtra("cedula", result);
                startActivity(intent);
            }
        }
    }

}
